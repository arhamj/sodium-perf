mod crypto;

use crate::crypto::{HexStringOrBuffer, ShardusCrypto};
use futures::future::join_all;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::sync::Arc;
use std::time::Instant;

#[tokio::main]
async fn main() -> io::Result<()> {
    non_parallel_bench();
    parallel_bench().await
}

fn non_parallel_bench() {
    // Initialize ShardusCrypto
    let sc = ShardusCrypto::new("69fa4195670576c0160d660c3be36556ff8d504725be8a59b5a96509e0c994bc"); // Replace "YOUR_KEY_HERE" with your key
    let sk = sodiumoxide::crypto::sign::SecretKey::from_slice(
            &sodiumoxide::hex::decode("c3774b92cc8850fb4026b073081290b82cab3c0f66cac250b4d710ee9aaf83ed8088b37f6f458104515ae18c2a05bde890199322f62ab5114d20c77bde5e6c9d").unwrap(),
        ).expect("Invalid secret key");
    let pk = sk.public_key();

    let path = Path::new("output.txt");
    let file = File::open(&path).expect("Could not open file");
    let reader = io::BufReader::new(file);

    let mut total_hash_time = 0.0;
    let mut total_sign_time = 0.0;
    let mut total_verify_time = 0.0;

    let mut first = true;

    for line in reader.lines() {
        let line = line.unwrap();
        if line == "JSON" {
            continue;
        }
        if first {
            print_verification_data(&line, &sc, &sk);
            first = false;
        }
        let json_as_bytes = line.as_bytes().to_vec();

        // Measure hashing
        let start_hash = Instant::now();
        let _hashed_data = sc.hash(&json_as_bytes, crypto::Format::Buffer);
        let duration_hash = start_hash.elapsed();
        total_hash_time += duration_hash.as_secs_f64();

        // Measure signing
        let start_sign = Instant::now();
        let signature = sc
            .sign(HexStringOrBuffer::Buffer(json_as_bytes.clone()), &sk)
            .expect("Failed to sign");
        let duration_sign = start_sign.elapsed();
        total_sign_time += duration_sign.as_secs_f64();

        // Measure verification
        let start_verify = Instant::now();
        let is_verified = sc.verify(&HexStringOrBuffer::Buffer(json_as_bytes), &signature, &pk);
        assert!(is_verified);
        let duration_verify = start_verify.elapsed();
        total_verify_time += duration_verify.as_secs_f64();
    }

    println!(
        "Average time for hashing: {} microseconds",
        (total_hash_time * 1_000_000.0) / 1000.0
    );
    println!(
        "Average time for signing: {} microseconds",
        (total_sign_time * 1_000_000.0) / 1000.0
    );
    println!(
        "Average time for verification: {} microseconds",
        (total_verify_time * 1_000_000.0) / 1000.0
    );
    println!(
        "Average time combined: {} microseconds",
        ((total_hash_time + total_sign_time + total_verify_time) * 1_000_000.0) / 1000.0
    );
}

fn print_verification_data(line: &str, sc: &ShardusCrypto, sk: &sodiumoxide::crypto::sign::SecretKey) {
    let json_as_bytes = line.as_bytes().to_vec();

    // Measure hashing
    let _hashed_data = sc.hash(&json_as_bytes, crypto::Format::Buffer);

    // Measure signing
    let signature = sc
        .sign(_hashed_data, &sk)
        .expect("Failed to sign");

    println!("Verification Signature: {}", sodiumoxide::hex::encode(signature));
}

async fn parallel_bench() -> io::Result<()> {
    let sc = Arc::new(ShardusCrypto::new(
        "69fa4195670576c0160d660c3be36556ff8d504725be8a59b5a96509e0c994bc",
    ));
    let sk = sodiumoxide::crypto::sign::SecretKey::from_slice(
            &sodiumoxide::hex::decode("c3774b92cc8850fb4026b073081290b82cab3c0f66cac250b4d710ee9aaf83ed8088b37f6f458104515ae18c2a05bde890199322f62ab5114d20c77bde5e6c9d").unwrap(),
        ).expect("Invalid secret key");
    let pk = sk.public_key();

    let path = Path::new("output.txt");
    let file = File::open(&path)?;
    let reader = io::BufReader::new(file);

    let total_time = Instant::now();
    let tasks: Vec<_> = reader
        .lines()
        .map(|line| {
            let line = line.expect("Failed to read line");
            let json_as_bytes = line.as_bytes().to_vec();

            let sc = Arc::clone(&sc);
            let sk = sk.clone();
            let pk = pk.clone();

            tokio::spawn(async move {
                let _hashed_data = sc.hash(&json_as_bytes, crypto::Format::Buffer);

                let signature = sc
                    .sign(HexStringOrBuffer::Buffer(json_as_bytes.clone()), &sk)
                    .expect("Failed to sign");

                let is_verified =
                    sc.verify(&HexStringOrBuffer::Buffer(json_as_bytes), &signature, &pk);
                assert!(is_verified);
            })
        })
        .collect();

    join_all(tasks).await;
    println!(
        "Total time for parallel operation: {} microseconds",
        (total_time.elapsed().as_secs_f32() * 1_000_000.0) / 1000.0
    );

    Ok(())
}
