import * as fs from 'fs'

// Function to generate a random string of a given length
function randomString(length: number): string {
  const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  let result = ''
  for (let i = 0; i < length; i++) {
    result += chars.charAt(Math.floor(Math.random() * chars.length))
  }
  return result
}

// Function to generate a random JSON of variable size
function generateRandomJSON(): object {
  const size = Math.floor(Math.random() * 300) + 1
  const json: any = {}
  for (let i = 0; i < size; i++) {
    json[randomString(5)] = randomString(5)
  }
  return json
}

// Main function to generate CSV data
function generateTXTData(): string {
  let csvData = 'JSON\n' // Header

  for (let id = 1; id <= 1000; id++) {
    const jsonString = JSON.stringify(generateRandomJSON())
    csvData += `${jsonString}\n`
  }

  return csvData
}

// Write the CSV data to a file
const csvData = generateTXTData()
fs.writeFileSync('output.txt', csvData, 'utf8')
console.log('TXT data saved to output.txt')
