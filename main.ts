import fs from 'fs'
import { performance } from 'perf_hooks'
import * as crypto from '@shardus/crypto-utils'

crypto.init('69fa4195670576c0160d660c3be36556ff8d504725be8a59b5a96509e0c994bc')

const keypair: crypto.Keypair = {
  publicKey: '8088b37f6f458104515ae18c2a05bde890199322f62ab5114d20c77bde5e6c9d',
  secretKey:
    'c3774b92cc8850fb4026b073081290b82cab3c0f66cac250b4d710ee9aaf83ed8088b37f6f458104515ae18c2a05bde890199322f62ab5114d20c77bde5e6c9d',
}

function sign<T>(obj: T): T & crypto.SignedObject {
  const objCopy = JSON.parse(crypto.stringify(obj))
  crypto.signObj(objCopy, keypair.secretKey, keypair.publicKey)
  return objCopy
}

function signWithSize<T>(obj: T): T & crypto.SignedObject {
  const wrappedMsgStr = crypto.stringify(obj)
  const msgLength = wrappedMsgStr.length
  //@ts-ignore
  obj.msgSize = msgLength
  return sign(obj)
}

async function main() {
  const data = fs.readFileSync('output.txt', 'utf-8')
  const lines = data.split('\n').filter((line) => line.trim() !== '')

  printVerificationData(lines)

  let totalSignTime = 0
  let totalVerifyTime = 0

  for (const line of lines) {
    if (line.startsWith('JSON')) {
      continue
    }
    const parsedObject = JSON.parse(line)
    const wrapperObject = {
      payload: parsedObject,
      size: 0,
    }

    const startSign = performance.now()
    const obj = signWithSize(wrapperObject)
    const durationSign = performance.now() - startSign

    const startVerify = performance.now()
    const isValid = crypto.verifyObj(obj)
    if (!isValid) {
      throw new Error('Invalid signature')
    }
    const durationVerify = performance.now() - startVerify

    totalSignTime += durationSign
    totalVerifyTime += durationVerify
  }

  console.log(`Average time for signing: ${totalSignTime / lines.length} milliseconds`)
  console.log(`Average time for verification: ${totalVerifyTime / lines.length} milliseconds`)
}

function printVerificationData(lines: string[]) {
  if (lines.length < 2) {
    return
  }
  const line = lines[1]
  const hash = crypto.hash(line)
  const signature = crypto.sign(hash, keypair.secretKey)

  console.log(`Verification Signature: ${signature}`)
}

main().catch((err) => {
  console.error('An error occurred:', err)
})
